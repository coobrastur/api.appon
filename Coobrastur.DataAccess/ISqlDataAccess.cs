﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Coobrastur.DataAccess
{
    public interface ISqlDataAccess
    {
        IDbConnection CreateConnection(string connectionStringName);

        void ExecuteScript(string script, IDbConnection connection, IDbTransaction transaction);

        IEnumerable<T> ExecuteScript<T>(string script, IDbConnection connection, IDbTransaction transaction);

        IEnumerable<T> ExecuteScript<T>(string script, string connectionStringName);

        List<T> LoadData<T>(string storedProcedure, string connectionStringName);

        List<T1> LoadData<T1, T2>(string storedProcedure, T2 parameters, string connectionStringName);

        void SaveData<T>(string storedProcedure, T parameters, string connectionStringName);

        void SaveData<T>(string storedProcedure, T parameters, IDbConnection connection, IDbTransaction transaction);
    }
}

﻿using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Coobrastur.DataAccess
{
    public class SqlDataAccess : ISqlDataAccess
    {
        private readonly IConfiguration _configuration;

        public SqlDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IDbConnection CreateConnection(string connectionStringName)
        {
            string connectionString = _configuration.GetConnectionString(connectionStringName);
            IDbConnection connection = new SqlConnection(connectionString);
            connection.Open();
            return connection;
        }

        public IEnumerable<T> ExecuteScript<T>(string script, string connectionStringName)
        {
            using IDbConnection connection = CreateConnection(connectionStringName);
            return ExecuteScript<T>(script, connection, null);
        }

        public void ExecuteScript(string script, IDbConnection connection, IDbTransaction transaction)
        {
            connection.Execute(
                sql: script,
                commandType: CommandType.Text,
                transaction: transaction);
        }

        public IEnumerable<T> ExecuteScript<T>(string script, IDbConnection connection, IDbTransaction transaction)
        {
            return connection.Query<T>(
                sql: script,
                commandType: CommandType.Text,
                transaction: transaction);
        }

        public List<T> LoadData<T>(string storedProcedure, string connectionStringName)
        {
            return LoadData<T, dynamic>(storedProcedure, null, connectionStringName);
        }

        public List<T1> LoadData<T1, T2>(string storedProcedure, T2 parameters, string connectionStringName)
        {
            using IDbConnection connection = CreateConnection(connectionStringName);
            List<T1> rows = connection.Query<T1>(
                sql: storedProcedure,
                param: parameters,
                commandType: CommandType.StoredProcedure).ToList();

            return rows;
        }

        public void SaveData<T>(string storedProcedure, T parameters, string connectionStringName)
        {
            using IDbConnection connection = CreateConnection(connectionStringName);
            SaveData(storedProcedure, parameters, connection, null);
        }

        public void SaveData<T>(string storedProcedure, T parameters, IDbConnection connection, IDbTransaction transaction)
        {
            connection.Execute(
                sql: storedProcedure,
                param: parameters,
                commandType: CommandType.StoredProcedure,
                transaction: transaction);
        }
    }
}

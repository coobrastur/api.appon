﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.AppOn.Api.Interfaces;
using Api.AppOn.Api.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.AppOn.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConsultaController : ControllerBase
    {
        public IConsultaService _consultaService;
        public ConsultaController(IConsultaService consultaService)
        {
            consultaService = _consultaService;
        }

        [HttpPost("InsertAppVendaOnline")]
        public ActionResult<object> InsertAppVendaOnline([FromBody] DadosContratoModel model)
        {
            try
            {
                var retorno = _consultaService.InsertAppVendaOnline(model);

                return retorno;
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
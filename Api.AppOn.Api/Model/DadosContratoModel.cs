﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.AppOn.Api.Model
{
    public class DadosContratoModel
    {
        public string CpfAssociado { get; set; }
        [JsonIgnore]
        public double AssCpfCnpj { get; set; }
        [JsonIgnore]
        public string Token { get; set; }
        [JsonIgnore]
        public char[] EndUf { get; set; }
        [JsonIgnore]
        public int AssNumCelular { get; set; }
        [JsonIgnore]
        public double CpfTitularCartao { get; set; }
        [JsonIgnore]
        public double CpfTitularContaCorrente { get; set; }
        [JsonIgnore]
        public int FormaAditamento { get; set; }
        [JsonIgnore]
        public double AssCpfTerceiro { get; set; }
        public string NomeAssociado { get; set; }
        public string UfAssociado { get; set; }
        public string EmailAssociado { get; set; }
        public string DddCelularAssociado { get; set; }
        public int CelularAssociado { get; set; }
        public bool Indicacao { get; set; }
        public string NomeIndicador { get; set; }
        public string CpfIndicador { get; set; }
        public string EmailIndicador { get; set; }
        public string CelularIndicador { get; set; }
        public int IdTipoVenda { get; set; }
        public int IdPlano { get; set; }
        public int DiariasPlano { get; set; }
        public bool FamiliaPlano { get; set; }
        public bool Adesao { get; set; }
        public decimal? ValorPagamentoAdesao { get; set; }
        public int? IdMeioPagamentoAdesao { get; set; }
        public int? IdFormaPagamentoAdesaoVendedor { get; set; }
        public int? ParcelasPagamentoAdesaoVendedor { get; set; }
        public string DataPagamentoAdesaoVendedor { get; set; }
        public string ObsAdesaoVendedor { get; set; }
        public int? IdFormaPagamentoAdesaoCoobrastur { get; set; }
        public int? ParcelasPagamentoAdesaoCoobrastur { get; set; }
        public string DataPagamentoAdesaoCoobrastur { get; set; }
        public string ObsAdesaoCoobrastur { get; set; }
        public bool Fiador { get; set; }
        public string CpfFiador { get; set; }
        public string NomeFiador { get; set; }
        public string DataNascimentoFiador { get; set; }
        public string NacionalidadeFiador { get; set; }
        public int? IdEstadoCivilFiador { get; set; }
        public string ConjugeFiador { get; set; }
        public string EmailFiador { get; set; }
        public string DddCelularFiador { get; set; }
        public string CelularFiador { get; set; }
        public string DddTelefoneFiador { get; set; }
        public string TelefoneFiador { get; set; }
        public int? IdFormaAssinaturaFiador { get; set; }
        public string AssinaturaFiador { get; set; }
        public int IdFormaPagamentoMensalidade { get; set; }
        public string CpfCartao { get; set; }
        public string NomeCartao { get; set; }
        public string NumeroCartao { get; set; }
        public string ValidadeCartao { get; set; }
        public string CvvCartao { get; set; }
        public int? DiaFaturaCartao { get; set; }
        public string CpfConta { get; set; }
        public string NomeConta { get; set; }
        public int? IdBancoConta { get; set; }
        public string NumeroConta { get; set; }
        public string DvConta { get; set; }
        public string AgenciaConta { get; set; }
        public int? DiaDebitoConta { get; set; }
        public bool Terceiro { get; set; }
        public string CpfTerceiro { get; set; }
        public string NomeTerceiro { get; set; }
        public string NascimentoTerceiro { get; set; }
        public string NacionalidadeTerceiro { get; set; }
        public int? UdEstadoCivilTerceiro { get; set; }
        public string ConjugeTerceiro { get; set; }
        public string MaeTerceiro { get; set; }
        public string PaiTerceiro { get; set; }
        public string EmailTerceiro { get; set; }
        public string DddCelularTerceiro { get; set; }
        public string CelularTerceiro { get; set; }
        public string DddTelefoneTerceiro { get; set; }
        public string TelefoneTerceiro { get; set; }
        public int? IdFormaAssinaturaTerceiro { get; set; }
        public string AssinaturaTerceiro { get; set; }
        public int IdFormaAssinaturaAssociado { get; set; }
        public string AssinaturaAssociado { get; set; }
        public bool RetricaoAssociado { get; set; }
        public int CodigoVendedor { get; set; }
        public bool Aditamento { get; set; }
        public int? IdFormaAditamento { get; set; }
        public int? IdBandeiraCartao { get; set; }
        public string UsuarioVendedor { get; set; }
        public string DddCelularIndicador { get; set; }
        public int? MesDebitoConta { get; set; }
        public int? AnoDebitoConta { get; set; }
        public int? MesFaturaCartao { get; set; }
        public int? AnoFaturaCartao { get; set; }
        public string ContratoString { get; set; }
        public string CepAssociado { get; set; }
        public string CidadeAssociado { get; set; }
        public string BairroAssociado { get; set; }
        public string RuaAssociado { get; set; }
        public string ComplementoRuaAssociado { get; set; }
        public string NumeroRuaAssociado { get; set; }
        public string DataVendaSemDados { get; set; }
        public int AppVenda { get; set; }
    }
}

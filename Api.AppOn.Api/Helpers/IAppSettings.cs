﻿namespace Api.AppOn.Api.Helpers
{
    public interface IAppSettings
    {
        public string TokenSecret { get; set; }
    }
}

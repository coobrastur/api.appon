﻿using Api.AppOn.Api.Interfaces;
using Api.AppOn.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.AppOn.Api.Services
{
    public class ConsultaService : IConsultaService
    {
        public object InsertAppVendaOnline(DadosContratoModel model)
        {
            //bool? suporte = false;
            //var crm = new CRMBL();

            //codigoVendedor = codigoVendedor == 0 ? crm.ObterCodigoVendedor(usuarioVendedor) : codigoVendedor;
            //if (nomeAssociado.Contains("/"))
            //{
            //    suporte = true;
            //    var usuarioSplit = nomeAssociado.Split('/');
            //    nomeAssociado = usuarioSplit[0];
            //}
            //try
            //{
            //    VendaExternaAppContrato vendaExternaAppContrato = new VendaExternaAppContrato();
            //    string strResponse = null;
            //    if (Context.Request.Headers.Get("Authorization") == null)
            //    {

            //        int idSuporte = -1;
            //        if (suporte.Value == false)
            //        {
            //            var dadosRetorno = crm.RetornaVendas(usuarioVendedor, codigoVendedor, Convert.ToDouble(cpfAssociado), nomeAssociado, contratoString, -1, suporte, "Token Incorreto");
            //            var retorno = dadosRetorno.Rows[0];
            //            idSuporte = Convert.ToInt32(retorno.ItemArray[0]);
            //        }

            //        var objectJsonBody = new
            //        {
            //            mensagem = "Token Incorreto",
            //            code = 11,
            //            idSuporte = idSuporte
            //        };

            //        strResponse = JsonConvert.SerializeObject(montaJson(objectJsonBody, new object(), 400, 11, "Token Incorreto"));
            //    }
            //    else
            //    {
            //        vendaExternaAppContrato.UsuarioVendedor = usuarioVendedor;
            //        vendaExternaAppContrato.Token = Context.Request.Headers.Get("Authorization");
            //        vendaExternaAppContrato.AssCPF_CNPJ = Convert.ToDouble(cpfAssociado);
            //        vendaExternaAppContrato.AassNome_RazaoSocial = nomeAssociado;
            //        vendaExternaAppContrato.EndUF = ufAssociado.ToCharArray();

            //        vendaExternaAppContrato.EndAssociado = ruaAssociado;
            //        vendaExternaAppContrato.nroEndAssociado = numeroRuaAssociado;
            //        vendaExternaAppContrato.complEndAssociado = complementoRuaAssociado;
            //        vendaExternaAppContrato.bairroAssociado = bairroAssociado;
            //        vendaExternaAppContrato.cidadeAssociado = cidadeAssociado;
            //        vendaExternaAppContrato.cepAssociado = cepAssociado;

            //        vendaExternaAppContrato.AssEmailPessoal = emailAssociado;
            //        vendaExternaAppContrato.AssNumCelularDDD = Convert.ToInt32(dddCelularAssociado);
            //        int intCelularAssociado = Convert.ToInt32(celularAssociado);
            //        vendaExternaAppContrato.AssNumCelular = intCelularAssociado;
            //        vendaExternaAppContrato.TipoVenda = idTipoVenda;
            //        vendaExternaAppContrato.Tpplcodigo = idPlano;
            //        vendaExternaAppContrato.PlQtdeDiarias = diariasPlano;
            //        vendaExternaAppContrato.Plfamilia = familiaPlano;
            //        //1 debito, 2 cartao
            //        vendaExternaAppContrato.FormaPagamento = idFormaPagamentoMensalidade;
            //        bool formaPagamentoCartao = idFormaPagamentoMensalidade == 2 || idFormaPagamentoMensalidade == 3;
            //        bool formaPagamentoBanco = idFormaPagamentoMensalidade == 1;

            //        vendaExternaAppContrato.CpfTitularCartao = !formaPagamentoCartao ? 0 : Convert.ToDouble(cpfCartao);
            //        vendaExternaAppContrato.NomeTitularCartao = !formaPagamentoCartao ? "" : nomeCartao;
            //        vendaExternaAppContrato.CarNumero = !formaPagamentoCartao ? "" : numeroCartao;
            //        vendaExternaAppContrato.CarValidade = !formaPagamentoCartao ? "" : validadeCartao;
            //        vendaExternaAppContrato.BanCodigo = !formaPagamentoCartao ? 0 : idBandeiraCartao;
            //        vendaExternaAppContrato.Carcodigo_seguranca = !formaPagamentoCartao ? 0 : Convert.ToInt32(cvvCartao);
            //        vendaExternaAppContrato.CarDia = !formaPagamentoCartao ? 0 : diaFaturaCartao;
            //        vendaExternaAppContrato.CpfTitularContaCorrente = !formaPagamentoBanco ? 0 : Convert.ToDouble(cpfConta);
            //        vendaExternaAppContrato.NomeTitularContaCorrente = !formaPagamentoBanco ? "" : nomeConta;
            //        vendaExternaAppContrato.BcoCodigo = !formaPagamentoBanco ? 0 : idBancoConta;

            //        string numeroContaCaixa = numeroConta.ToString().Replace("null", "");
            //        if (!numeroContaCaixa.ToLower().Equals("null") && idBancoConta == 104)
            //        {
            //            string operador = numeroConta.Substring(0, 3) == "128" ? "  3" : numeroConta.ToString().Substring(0, 3);
            //            string conta = numeroConta.Substring(0, 3) == "128" ? numeroConta.ToString().Substring(4, numeroConta.ToString().Length - 4) : numeroConta.ToString().Substring(3, numeroConta.ToString().Length - 3);
            //            numeroContaCaixa = operador + conta;
            //        }


            //        vendaExternaAppContrato.ConContaCorrente = !formaPagamentoBanco ? "" : numeroContaCaixa;
            //        vendaExternaAppContrato.ConContaCorrenteDV = !formaPagamentoBanco ? "" : dvConta;
            //        vendaExternaAppContrato.ConAgencia = !formaPagamentoBanco ? "" : agenciaConta.ToString();
            //        vendaExternaAppContrato.ConAgencia = !formaPagamentoBanco ? "" : agenciaConta.ToString();
            //        vendaExternaAppContrato.DebDia = !formaPagamentoBanco ? 0 : diaDebitoConta;
            //        vendaExternaAppContrato.FormaAssinatura = idFormaAssinaturaAssociado;
            //        vendaExternaAppContrato.Assinatura = assinaturaAssociado;
            //        vendaExternaAppContrato.AssRestricao = retricaoAssociado;

            //        vendaExternaAppContrato.VendCodigo = codigoVendedor;
            //        vendaExternaAppContrato.Aditamento = aditamento;
            //        vendaExternaAppContrato.FormaAditamento = !aditamento ? 0 : idFormaAditamento;


            //        // ASSOCIADO - INDICAÇÃO
            //        vendaExternaAppContrato.Indicacao = indicacao;
            //        vendaExternaAppContrato.NomeIndicador = !indicacao ? null : nomeIndicador;
            //        vendaExternaAppContrato.CpfIndicador = !indicacao || indicacao && string.IsNullOrEmpty(cpfIndicador) ? 0 : Convert.ToDouble(cpfIndicador);
            //        vendaExternaAppContrato.EmailIndicador = !indicacao || indicacao && string.IsNullOrEmpty(emailIndicador) ? null : emailIndicador;
            //        vendaExternaAppContrato.CelularIndicador = !indicacao || indicacao && string.IsNullOrEmpty(celularIndicador) ? null : celularIndicador.ToString();

            //        // ASSOCIADO - ADESAO
            //        DateTime? dataPagAdesaoVendedor = null;
            //        DateTime? dataPagAdesaoCoobrastur = null;
            //        if (!string.IsNullOrEmpty(dataPagamentoAdesaoVendedor))
            //        {
            //            dataPagAdesaoVendedor = Convert.ToDateTime(dataPagamentoAdesaoVendedor);
            //        }

            //        if (!string.IsNullOrEmpty(dataPagamentoAdesaoCoobrastur))
            //        {
            //            dataPagAdesaoCoobrastur = Convert.ToDateTime(dataPagamentoAdesaoCoobrastur);
            //        }

            //        vendaExternaAppContrato.Adesao = adesao;
            //        vendaExternaAppContrato.ValorAdesao = !adesao ? null : valorPagamentoAdesao;
            //        vendaExternaAppContrato.MeioAdesao = !adesao ? null : idMeioPagamentoAdesao;
            //        vendaExternaAppContrato.TipoAdesaoVendedor = !adesao ? null : idFormaPagamentoAdesaoVendedor;
            //        vendaExternaAppContrato.FormaAdesaoVendedor = !adesao ? null : parcelasPagamentoAdesaoVendedor;
            //        vendaExternaAppContrato.DataVendedor = dataPagAdesaoVendedor;
            //        vendaExternaAppContrato.ObsVendedor = !adesao ? null : obsAdesaoVendedor;
            //        vendaExternaAppContrato.TipoAdesaoCoobrastur = !adesao ? null : idFormaPagamentoAdesaoCoobrastur;
            //        vendaExternaAppContrato.FormaAdesaoCoobrastur = !adesao ? null : parcelasPagamentoAdesaoCoobrastur;
            //        vendaExternaAppContrato.DataCoobrastur = dataPagAdesaoCoobrastur;
            //        vendaExternaAppContrato.ObsCoobrastur = !adesao ? null : obsAdesaoCoobrastur;
            //        if (!adesao && idFormaPagamentoMensalidade != 9)
            //        {

            //            string dataProgamada = formaPagamentoBanco ?
            //                string.Format("{0}/{1}/{2}", diaDebitoConta, mesDebitoConta, anoDebitoConta) :
            //                idFormaPagamentoMensalidade != 4 ? string.Format("{0}/{1}/{2}", diaFaturaCartao, mesFaturaCartao, anoFaturaCartao) : string.Format("{0}/{1}/{2}", bolDia, bolMes, bolAno);

            //            if ((mesDebitoConta == 0 && anoDebitoConta == 0) && (mesFaturaCartao == 0 && mesFaturaCartao == 0))
            //            {
            //                dataProgamada = dataPagamentoAdesaoCoobrastur;
            //            }

            //            vendaExternaAppContrato.DataProgramada = Convert.ToDateTime(dataProgamada);
            //        }

            //        DateTime? dataVendaSemDado = null;
            //        if (!string.IsNullOrEmpty(dataVendaSemDados))
            //        {
            //            dataVendaSemDado = Convert.ToDateTime(dataVendaSemDados);
            //        }
            //        if (vendaExternaAppContrato.DataProgramada == null && (vendaExternaAppContrato.DataCoobrastur != null || vendaExternaAppContrato.DataVendedor != null || dataVendaSemDado != null))
            //        {
            //            vendaExternaAppContrato.DataProgramada =
            //                vendaExternaAppContrato.DataCoobrastur != null ? vendaExternaAppContrato.DataCoobrastur
            //                : vendaExternaAppContrato.DataVendedor != null ? vendaExternaAppContrato.DataVendedor
            //                : dataVendaSemDado != null ? dataVendaSemDado : null;
            //        }

            //        // ASSOCIADO - FIADOR
            //        DateTime? dtNascimentoFiador = null;
            //        if (!string.IsNullOrEmpty(dataNascimentoFiador))
            //        {
            //            dtNascimentoFiador = Convert.ToDateTime(dataNascimentoFiador);
            //        }
            //        vendaExternaAppContrato.Fiador = fiador;
            //        vendaExternaAppContrato.FiaCpf = !fiador ? 0 : Convert.ToDouble(cpfFiador);
            //        vendaExternaAppContrato.Fianome = !fiador ? null : nomeFiador;
            //        vendaExternaAppContrato.FiaDtNascimento = dtNascimentoFiador;
            //        vendaExternaAppContrato.Fianacionalidade = !fiador ? null : nacionalidadeFiador;
            //        vendaExternaAppContrato.CivCodigo = !fiador ? null : idEstadoCivilFiador;
            //        vendaExternaAppContrato.FiaConjuge = !fiador ? null : conjugeFiador;
            //        vendaExternaAppContrato.FiaConjugeFiliacao = null;
            //        vendaExternaAppContrato.FiaConjugeFiliacao2 = null;
            //        vendaExternaAppContrato.FiaEmailPessoal = !fiador ? null : emailFiador;
            //        vendaExternaAppContrato.FiaNumeroCelularDDD = !fiador ? null : dddCelularFiador.ToString();
            //        vendaExternaAppContrato.FiaNumeroCelular = !fiador ? null : celularFiador.ToString();
            //        vendaExternaAppContrato.FiaddFone = !fiador ? null : dddCelularFiador.ToString();
            //        vendaExternaAppContrato.FiaFone = !fiador ? null : celularFiador.ToString();
            //        vendaExternaAppContrato.FormaAssinaturaFiador = !fiador ? null : idFormaAssinaturaFiador;
            //        vendaExternaAppContrato.AssinaturaFiador = !fiador ? null : assinaturaFiador;
            //        vendaExternaAppContrato.Reativacao = TpplanoReativado == null ? 0 : 1;
            //        vendaExternaAppContrato.TpplcodigoReativado = TpplanoReativado;
            //        vendaExternaAppContrato.PlcodigoReativado = plCodigoReativado;
            //        vendaExternaAppContrato.BolCpf = idFormaPagamentoMensalidade == 4 ? Convert.ToDouble(bolCpf) : 0;
            //        vendaExternaAppContrato.BolDia = bolDia;
            //        vendaExternaAppContrato.BolMes = bolMes;
            //        vendaExternaAppContrato.BolAno = bolAno;
            //        vendaExternaAppContrato.Campanha = campanha;
            //        var dadosExistente = crm.AssociadoExistente(Convert.ToDouble(cpfAssociado));

            //        vendaExternaAppContrato.UsurioExistente = Convert.ToInt32(dadosExistente.Rows[0].ItemArray[0]) != -1 ? true : false;

            //        var dadosRetorno = crm.InsertVendaExternaAppOnline(vendaExternaAppContrato);
            //        var rows = dadosRetorno.Tables[5];
            //        var retorno = dadosRetorno.Tables[dadosRetorno.Tables.Count - 1].Rows[0];
            //        var objectJsonBody = new
            //        {
            //            mensagem = "Usuário cadastrado com sucesso!",
            //            idVenda = retorno.ItemArray[0]
            //        };

            //        strResponse = JsonConvert.SerializeObject(montaJson(objectJsonBody, new object(), 200, 0, ""));

            //        var retornoVenda = crm.RetornaVendas(usuarioVendedor, codigoVendedor, Convert.ToDouble(cpfAssociado), nomeAssociado, contratoString,
            //            Convert.ToInt32(retorno.ItemArray[0]), suporte, "");

            //        //crm.AtualizaStatus(appVenda);
            //        if (idFormaPagamentoMensalidade == 4)
            //        {
            //            var dias = vendaExternaAppContrato.DataProgramada.Value.Date - DateTime.Now.Date;
            //            bool boletoImediato = dias.Days <= 7 ? true : false;
            //            if (boletoImediato)
            //            {
            //                D4SignBL objD4SignDAO = new D4SignBL();
            //                var valor = objD4SignDAO.ValorMensaliidde(Convert.ToInt32(vendaExternaAppContrato.Tpplcodigo), Convert.ToBoolean(vendaExternaAppContrato.Plfamilia), Convert.ToInt32(vendaExternaAppContrato.PlQtdeDiarias));

            //                string endereco = string.Format("{0}, {2}, {1}", vendaExternaAppContrato.EndAssociado, vendaExternaAppContrato.nroEndAssociado, vendaExternaAppContrato.nroEndAssociado);

            //                string link = GeraBoleto(vendaExternaAppContrato.AassNome_RazaoSocial, cpfAssociado, Convert.ToDouble(valor.Tables[0].Rows[0].ItemArray[0].ToString()), Convert.ToDateTime(string.Format("{0}/{1}/{2}", bolDia, bolMes, bolAno)), endereco, vendaExternaAppContrato.bairroAssociado, vendaExternaAppContrato.cidadeAssociado, ufAssociado, vendaExternaAppContrato.cepAssociado, rows.Rows[0].ItemArray[0].ToString(), rows.Rows[0].ItemArray[2].ToString(), rows.Rows[0].ItemArray[3].ToString(), vendaExternaAppContrato.AssEmailPessoal);

            //            }
            //            else
            //            {
            //                EnviarEmailLink(2, vendaExternaAppContrato.AssEmailPessoal, vendaExternaAppContrato.AassNome_RazaoSocial, Convert.ToInt32(rows.Rows[0].ItemArray[0].ToString()), Convert.ToInt32(rows.Rows[0].ItemArray[2].ToString()), Convert.ToInt32(rows.Rows[0].ItemArray[3].ToString()));
            //            }

            //        }

            //        string strCallback = Context.Request.QueryString["callback"];
            //        strResponse = strCallback + strResponse;
            //        Context.Response.Clear();
            //        Context.Response.ContentType = "application/json; charset=utf-8";
            //        Context.Response.Flush();
            //        Context.Response.Write(strResponse);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    #region erros
            //    var objetAbstract = new object();
            //    string strResponse = "";
            //    if (ex.Message.ToLower() == "token incorreto")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 1, "Token Incorreto"));
            //    }
            //    else if (ex.Message.ToLower() == "cobranca de adesao incorreta")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 2, "Cobrança de Adesao Incorreta"));
            //    }
            //    else if (ex.Message.ToLower() == "nao pode cobrar adesao")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 3, "Não Pode Cobrar Adesao"));
            //    }
            //    else if (ex.Message.ToLower() == "tipo de venda recuperacao incorreta")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 4, "Tipo de Venda Recuperacao Incorreta"));
            //    }
            //    else if (ex.Message.ToLower() == "necessario cobrar adesao")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 5, "Necessário Cobrar Adesão"));
            //    }
            //    else if (ex.Message.ToLower() == "obrigatorio aditamento ou fiador")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 6, "Obrigatório Aditamento ou Fiador"));
            //    }
            //    else if (ex.Message.ToLower() == "cpf indicador incorreto")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 7, "CPF Indicador Incorreto"));
            //    }
            //    else if (ex.Message.ToLower() == "cartao de credito invalido ou inapto")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 8, "Cartão de Crédito Inválido ou Inapto"));
            //    }
            //    else if (ex.Message.ToLower() == "fiador inadimplente")
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 9, "Fiador Inadimplente"));
            //    }
            //    else if (ex.Message.ToLower().Contains("erro! usuário não pode realizar vendas!"))
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 10, "Usuário não pode realizar vendas!"));
            //    }
            //    else
            //    {
            //        strResponse = JsonConvert.SerializeObject(montaJson(objetAbstract, objetAbstract, 400, 11, ex.Message));
            //    }

            //    int idSuporte = -1;
            //    if (suporte.Value == false)
            //    {
            //        var dadosRetorno = crm.RetornaVendas(usuarioVendedor, codigoVendedor, Convert.ToDouble(cpfAssociado), nomeAssociado, contratoString, -1, suporte, ex.Message);
            //        var retorno = dadosRetorno.Rows[0];
            //        idSuporte = Convert.ToInt32(retorno.ItemArray[0]);
            //    }

            //    var objectJsonBody = new
            //    {
            //        mensagem = ex.Message,
            //        code = 11,
            //        idSuporte = idSuporte
            //    };

            //    strResponse = JsonConvert.SerializeObject(montaJson(objectJsonBody, objetAbstract, 400, 11, ex.Message));

            //    string strCallback = Context.Request.QueryString["callback"];
            //    strResponse = strCallback + strResponse;
            //    Context.Response.Clear();
            //    Context.Response.ContentType = "application/json; charset=utf-8";
            //    Context.Response.Flush();
            //    Context.Response.Write(strResponse);
            //    #endregion
            //}
            //finally
            //{
            //    Context.Response.End();
            //}
            return new object();
        }
    }
}

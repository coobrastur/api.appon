﻿namespace Api.AppOn.Api.Authentication
{
    public interface IRefreshTokenGenerator
    {
        public string GenerateToken(string username, string password, string tokenGerado);
    }
}
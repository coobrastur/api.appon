﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Api.AppOn.Api.Authentication
{
    public interface IJWTAuthenticationManager
    {
        public AuthenticationResponse Authenticate(string username, string password, string token);

        public IDictionary<string, string> UsersRefreshTokens { get; set; }

        public AuthenticationResponse Authenticate(string username, Claim[] claims);
    }
}
﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Api.AppOn.Api.Authentication
{
    public class RefreshTokenGenerator : IRefreshTokenGenerator
    {
        public string GenerateToken(string username, string password, string tokenGerado)
        {
            var randomNumber = Encoding.UTF8.GetBytes(password + password + tokenGerado);

            using (var randomNumberGenerator = RandomNumberGenerator.Create())
            {
                randomNumberGenerator.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }
    }
}
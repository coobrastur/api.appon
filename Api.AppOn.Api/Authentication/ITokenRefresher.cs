﻿namespace Api.AppOn.Api.Authentication
{
    public interface ITokenRefresher
    {
        AuthenticationResponse Refresh(RefreshCred refreshCred);
    }
}
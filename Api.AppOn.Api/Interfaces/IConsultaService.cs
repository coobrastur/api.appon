﻿using Api.AppOn.Api.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.AppOn.Api.Interfaces
{
    public interface IConsultaService
    {
        public object InsertAppVendaOnline(DadosContratoModel model);
    }
}

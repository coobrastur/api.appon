﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Coobrastur.Library
{
    public static class Extensions
    {
        #region Generics

        public static void ThrowIfNull<T>(this T value, string propertyName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(propertyName);
            }
        }

        #endregion Generics

        #region Enum

        public static string ToDescription(this Enum @enum)
        {
            @enum.ThrowIfNull(nameof(@enum));
            FieldInfo fi = @enum.GetType().GetField(@enum.ToString());

            DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];

            if (attributes != null && attributes.Any())
            {
                return attributes.First().Description;
            }

            return @enum.ToString();
        }

        #endregion Enum
    }
}
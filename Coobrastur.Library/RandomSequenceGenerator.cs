﻿using System;
using System.Globalization;
using System.Text;

namespace Coobrastur.Library
{
    public class RandomSequenceGenerator : IRandomSequenceGenerator
    {
        private const string _sequenceChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        private Random _random;

        public RandomSequenceGenerator()
        {
            _random = new Random();
        }

        public string Generate(int length)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < length; i++)
            {
                int index = _random.Next(_sequenceChars.Length);
                sb.Append(_sequenceChars[index].ToString(CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }
    }
}
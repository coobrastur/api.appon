﻿namespace Coobrastur.Library
{
    public interface IRandomSequenceGenerator
    {
        string Generate(int length);
    }
}